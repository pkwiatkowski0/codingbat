package map2;

import java.util.HashMap;

/**
 * Created by patryk on 20.06.2017.
 */
/*
The classic word-count algorithm: given an array of strings, return a Map<String, Integer> with a key for each different string, with the value the number of times that string appears in the array.

wordCount(["a", "b", "a", "c", "b"]) → {"a": 2, "b": 2, "c": 1}
wordCount(["c", "b", "a"]) → {"a": 1, "b": 1, "c": 1}
wordCount(["c", "c", "c", "c"]) → {"c": 4} */
public class WordCount {
    public HashMap<String, Integer> wordCount(String[] strings) {
        HashMap<String, Integer> map = new HashMap<>();
        int value = 1;
        for (String str : strings) {
            if (map.containsKey(str)) {
                value = map.get(str) + 1;
                map.put(str, value);
            } else {
                map.put(str, 1);
            }
        }
        return map;
    }
}
