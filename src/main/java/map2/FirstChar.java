package map2;

import java.util.HashMap;

/**
 * Created by patryk on 20.06.2017.
 */
/*
Given an array of non-empty strings, return a Map<String, String> with a key for every different first character seen, with the value of all the strings starting with that character appended together in the order they appear in the array.

firstChar(["salt", "tea", "soda", "toast"]) → {"s": "saltsoda", "t": "teatoast"}
firstChar(["aa", "bb", "cc", "aAA", "cCC", "d"]) → {"a": "aaaAA", "b": "bb", "c": "cccCC", "d": "d"}
firstChar([]) → {}*/
public class FirstChar {
    public HashMap<String, String> firstChar(String[] strings) {
        HashMap<String, String> map = new HashMap<>();
        for (String str : strings) {
//            String key = str.substring(0, 1);
            String key = String.valueOf(str.charAt(0));
            if (map.containsKey(key)) {
                String val = map.get(key) + str;
                map.put(key, val);
            } else {
                map.put(key, str);
            }
        }

        return map;
    }
}
