package map2;

import java.util.HashMap;

/**
 * Created by patryk on 20.06.2017.
 */
/*
Loop over the given array of strings to build a result string like this: when a string appears the 2nd, 4th, 6th, etc. time in the array, append the string to the result. Return the empty string if no string appears a 2nd time.

wordAppend(["a", "b", "a"]) → "a"
wordAppend(["a", "b", "a", "c", "a", "d", "a"]) → "aa"
wordAppend(["a", "", "a"]) → "a"
*/
public class wordAppend {
    public String wordAppend(String[] strings) {
        HashMap<String, Integer> map = new HashMap<>();

        String result = "";
        for (String str : strings) {

            if (map.containsKey(str)) {
                int value = map.get(str);
                value ++;
                if (value % 2 == 0) {
                    result += str;
                }
                map.put(str, value);
            } else {
                map.put(str, 1);
            }
        }
        return result;
    }
}
