import java.util.HashMap;
import java.util.Map;

/**
 * Created by patryk on 04.06.2017.
 */
/*Given an array of non-empty strings, create and return a Map<String, String> as follows:
for each string add its first character as a key with its last character as the value.

pairs(["code", "bug"]) → {"b": "g", "c": "e"}
pairs(["man", "moon", "main"]) → {"m": "n"}
pairs(["man", "moon", "good", "night"]) → {"g": "d", "m": "n", "n": "t"}*/
public class Solution8 {
    public Map<String, String> pairs(String[] strings) {
        String firstLetter = "";
        String lastLetter = "";
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < strings.length; i++) {
            firstLetter = strings[i].substring(0, 1);
            lastLetter = strings[i].substring(strings[i].length() - 1);
            map.put(firstLetter, lastLetter);
            System.out.println("To jest piersza litera " + firstLetter);
            System.out.println("To jest ostatnia litera " + lastLetter);

        }
        return map;
    }

    public static void main(String[] args) {
        Solution7 solution7 = new Solution7();
        System.out.println(solution7.wordLen(new String[]{"code", "bug"}));
        System.out.println(solution7.wordLen(new String[]{"man", "moon", "main"}));
        System.out.println(solution7.wordLen(new String[]{"man", "moon", "good", "night"}));

    }
}
