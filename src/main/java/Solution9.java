/**
 * Created by patryk on 04.06.2017.
 */
/*Write implementation of myMethod, which should return arry (zero based index) of Strings containing String representation of numbers from 1 to 100. There are few exceptions:
-For numbers that are multiples of 3 array should contain string "Green"
-For numbers that are multiples of 5 array should contain string "Blue"
-For numbers that are multiples of 3 and 5 array should contain string "GreenBlue"*/
public class Solution9 {

    public String[] myMethod() {
        String[] result = new String[101];
        for (int i = 1; i < result.length; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
//                System.out.println("GreenBlue");
                result[i] = "GreenBlue";
            } else if (i % 3 == 0) {
//                System.out.println("Green");
                result[i] = "Green";
            } else if (i % 5 == 0) {
//                System.out.println("Blue");
                result[i] = "Blue";
            } else {
                result[i] = Integer.toString(i);
            }
        }
        return result;
    }


    public void iteration(String[] array) {
        for (String read :
                array) {
            System.out.println(read);
        }
    }

    public static void main(String[] args) {

        Solution9 solution = new Solution9();
        System.out.println("array has " + solution.myMethod().length + " elements.");
//        System.out.println("String array: " + solution.myMethod());
        solution.iteration(solution.myMethod());

    }
}
