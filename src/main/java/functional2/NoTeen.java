package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by patryk on 17.06.2017.
 */
/*Given a list of integers, return a list of those numbers, omitting any that are between 13 and 19 inclusive.

noTeen([12, 13, 19, 20]) → [12, 20]
noTeen([1, 14, 1]) → [1, 1]
noTeen([15]) → []
*/
public class NoTeen {
    public List<Integer> noTeen(List<Integer> nums) {
        nums.removeIf(n -> n >= 13 && n <= 19);
        return nums;
    }

    public static void main(String[] args) {
        List<Integer> nums1 = new ArrayList(Arrays.asList(1, 29, 3));
        List<Integer> nums2 = new ArrayList(Arrays.asList(12, 13, 19, 20));
        List<Integer> nums3 = new ArrayList(Arrays.asList(15));
        List<Integer> nums4 = new ArrayList(Arrays.asList());
        List<Integer> nums5 = new ArrayList(Arrays.asList(1, 14, 1));
        List<Integer> nums6 = new ArrayList(Arrays.asList(-1, -29, -3, -99, -1));
        List<Integer> nums7 = new ArrayList(Arrays.asList(13, 19));
        NoTeen object = new NoTeen();
        System.out.println(object.noTeen(nums1));
        System.out.println(object.noTeen(nums2));
        System.out.println(object.noTeen(nums3));
        System.out.println(object.noTeen(nums4));
        System.out.println(object.noTeen(nums5));
        System.out.println(object.noTeen(nums6));
        System.out.println(object.noTeen(nums7));
    }

}
