package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of strings, return a list where each string has "y" added at its end, omitting any resulting strings that contain "yy" as a substring anywhere.

noYY(["a", "b", "c"]) → ["ay", "by", "cy"]
noYY(["a", "b", "cy"]) → ["ay", "by"]
noYY(["xx", "ya", "zz"]) → ["xxy", "yay", "zzy"]
*/
public class NoYY {
    public List<String> noYY(List<String> strings) {
        List<String> stringsY = new ArrayList<>();
        stringsY = strings.stream().map(n -> n + "y").collect(Collectors.toList());
        stringsY.removeIf(n -> n.contains("yy"));
        return stringsY;
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("ay", "bb", "cccy"));
        List<String> strings2 = new ArrayList(Arrays.asList("a", "bb", "ccc", "dddd"));
        List<String> strings3 = new ArrayList(Arrays.asList("yyx", "y", "zzz"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("this", "not", "too", "long"));
        List<String> strings6 = new ArrayList(Arrays.asList("xx", "yy", "zz"));
        NoYY object = new NoYY();
        System.out.println(object.noYY(strings1));
        System.out.println(object.noYY(strings2));
        System.out.println(object.noYY(strings3));
        System.out.println(object.noYY(strings4));
        System.out.println(object.noYY(strings5));
        System.out.println(object.noYY(strings6));
    }

}
