package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of strings, return a list of the strings, omitting any string length 4 or more.

noLong(["this", "not", "too", "long"]) → ["not", "too"]
noLong(["a", "bbb", "cccc"]) → ["a", "bbb"]
noLong(["cccc", "cccc", "cccc"]) → []
*/
public class NoLong {
    public List<String> noLong(List<String> strings) {
        strings.removeIf(n -> n.length() >= 4);
        return strings;
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("a", "bb", "ccc"));
        List<String> strings2 = new ArrayList(Arrays.asList("a", "bb", "ccc", "dddd"));
        List<String> strings3 = new ArrayList(Arrays.asList("ccc", "dddd", "apple"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("this", "not", "too", "long"));
        List<String> strings6 = new ArrayList(Arrays.asList("aaaa", "bbb", "**", "33333"));
        NoLong object = new NoLong();
        System.out.println(object.noLong(strings1));
        System.out.println(object.noLong(strings2));
        System.out.println(object.noLong(strings3));
        System.out.println(object.noLong(strings4));
        System.out.println(object.noLong(strings5));
        System.out.println(object.noLong(strings6));
    }

}
