package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*Given a list of integers, return a list of those numbers squared and the product added to 10, omitting any of the resulting numbers that end in 5 or 6.

Square56([3, 1, 4]) → [19, 11]
Square56([1]) → [11]
Square56([2]) → [14]
*/
public class Square56 {
    public List<Integer> square56(List<Integer> nums) {
        List<Integer> nums2 = new ArrayList<>();
        nums2 = nums.stream().map(n -> n * n + 10).collect(Collectors.toList());
        nums2.removeIf(n -> n % 10 == 5 || n % 10 == 6);
        return nums2;
    }

    public static void main(String[] args) {
        List<Integer> nums1 = new ArrayList(Arrays.asList(3, 1, 4));
        List<Integer> nums2 = new ArrayList(Arrays.asList(1));
        List<Integer> nums3 = new ArrayList(Arrays.asList(2));
        List<Integer> nums4 = new ArrayList(Arrays.asList(3));
        List<Integer> nums5 = new ArrayList(Arrays.asList());
        List<Integer> nums6 = new ArrayList(Arrays.asList(59));
        List<Integer> nums7 = new ArrayList(Arrays.asList(3, -1, -4, 1, 5, 9));
        Square56 object = new Square56();
        System.out.println(object.square56(nums1));
        System.out.println(object.square56(nums2));
        System.out.println(object.square56(nums3));
        System.out.println(object.square56(nums4));
        System.out.println(object.square56(nums5));
        System.out.println(object.square56(nums6));
        System.out.println(object.square56(nums7));
    }

}
