package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by patryk on 17.06.2017.
 */
/*Given a list of strings, return a list of the strings, omitting any string length 3 or 4.

no34(["a", "bb", "ccc"]) → ["a", "bb"]
no34(["a", "bb", "ccc", "dddd"]) → ["a", "bb"]
no34(["ccc", "dddd", "apple"]) → ["apple"]
*/
public class No34 {
    public List<String> no34(List<String> strings) {
        strings.removeIf(n -> n.length() == 3 || n.length() == 4);
        return strings;
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("a", "bb", "ccc"));
        List<String> strings2 = new ArrayList(Arrays.asList("a", "bb", "ccc", "dddd"));
        List<String> strings3 = new ArrayList(Arrays.asList("ccc", "dddd", "apple"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("this", "not", "too", "long"));
        List<String> strings6 = new ArrayList(Arrays.asList("aaaa", "bbb", "**", "33333"));
        No34 object = new No34();
        System.out.println(object.no34(strings1));
        System.out.println(object.no34(strings2));
        System.out.println(object.no34(strings3));
        System.out.println(object.no34(strings4));
        System.out.println(object.no34(strings5));
        System.out.println(object.no34(strings6));
    }
}
