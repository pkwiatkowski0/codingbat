package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of non-negative integers, return a list of those numbers except omitting any that end with 9. (Note: % by 10)

No9([1, 2, 19]) → [1, 2]
No9([9, 19, 29, 3]) → [3]
No9([1, 2, 3]) → [1, 2, 3]*/
public class No9 {
    public List<Integer> no9(List<Integer> nums) {
        nums.removeIf(n -> n % 10 == 9);
        return nums;
    }

    public static void main(String[] args) {
        List<Integer> nums1 = new ArrayList(Arrays.asList(1, 29, 3));
        List<Integer> nums2 = new ArrayList(Arrays.asList(6, 8, 6, 9, 1));
        List<Integer> nums3 = new ArrayList(Arrays.asList(9));
        List<Integer> nums4 = new ArrayList(Arrays.asList());
        List<Integer> nums5 = new ArrayList(Arrays.asList(9, 10));
        List<Integer> nums6 = new ArrayList(Arrays.asList(-1, -29, -3, -99, -1));
        List<Integer> nums7 = new ArrayList(Arrays.asList(45, 99, 90, 28, 13, 999, 0));
        No9 object = new No9();
        System.out.println(object.no9(nums1));
        System.out.println(object.no9(nums2));
        System.out.println(object.no9(nums3));
        System.out.println(object.no9(nums4));
        System.out.println(object.no9(nums5));
        System.out.println(object.no9(nums6));
        System.out.println(object.no9(nums7));
    }
}
