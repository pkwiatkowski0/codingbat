package functional2;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*Given a list of non-negative integers, return a list of those numbers multiplied by 2, omitting any of the resulting numbers that end in 2.

two2([1, 2, 3]) → [4, 6]
two2([2, 6, 11]) → [4]
two2([0]) → [0]
*/
public class Two2 {
    public List<Integer> two2(List<Integer> nums) {
        List<Integer> nums2 = nums.stream().map(n -> n * 2).collect(Collectors.toList());
        nums2.removeIf(n -> n % 10 == 2);
        return nums2;
    }

    public static void main(String[] args) {
        List<Integer> nums1 = new ArrayList(Arrays.asList(1, 2, 3));
        List<Integer> nums2 = new ArrayList(Arrays.asList(2, 6, 11));
        List<Integer> nums3 = new ArrayList(Arrays.asList(0));
        List<Integer> nums4 = new ArrayList(Arrays.asList(1, 11, 111, 16));
        List<Integer> nums5 = new ArrayList(Arrays.asList());
        List<Integer> nums6 = new ArrayList(Arrays.asList(2, 3, 5, 7, 11));
        List<Integer> nums7 = new ArrayList(Arrays.asList(3, 1, 4, 1, 6, 99, 0));
        Two2 object = new Two2();
        System.out.println(object.two2(nums1));
        System.out.println(object.two2(nums2));
        System.out.println(object.two2(nums3));
        System.out.println(object.two2(nums4));
        System.out.println(object.two2(nums5));
        System.out.println(object.two2(nums6));
        System.out.println(object.two2(nums7));
    }
}
