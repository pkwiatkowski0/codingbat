package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of strings, return a list of the strings, omitting any string that contains a "z". (Note: the str.contains(x) method returns a boolean)

noZ(["aaa", "bbb", "aza"]) → ["aaa", "bbb"]
noZ(["hziz", "hzello", "hi"]) → ["hi"]
noZ(["hello", "howz", "are", "youz"]) → ["hello", "are"]*/
public class NoZ {
    public List<String> noZ(List<String> strings) {
        strings.removeIf(n -> n.contains("z"));
        return strings;
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("aaa", "bbb", "aza"));
        List<String> strings2 = new ArrayList(Arrays.asList("hziz", "hzello", "hi"));
        List<String> strings3 = new ArrayList(Arrays.asList("hello", "howz", "are", "youz"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("this", "not", "too", "long"));
        List<String> strings6 = new ArrayList(Arrays.asList("xx", "yy", "zz"));
        NoYY object = new NoYY();
        System.out.println(object.noYY(strings1));
        System.out.println(object.noYY(strings2));
        System.out.println(object.noYY(strings3));
        System.out.println(object.noYY(strings4));
        System.out.println(object.noYY(strings5));
        System.out.println(object.noYY(strings6));
    }
}
