package functional2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of integers, return a list of the integers, omitting any that are less than 0.

NoNeg([1, -2]) → [1]
NoNeg([-3, -3, 3, 3]) → [3, 3]
NoNeg([-1, -1, -1]) → []
*/
public class NoNeg {
    public List<Integer> noNeg(List<Integer> nums) {
        nums.removeIf(n -> n < 0);
        return nums;
    }

    public static void main(String[] args) {
        List<Integer> nums1 = new ArrayList(Arrays.asList(1, -2, 3));
        List<Integer> nums2 = new ArrayList(Arrays.asList(6, 8, 6, -9, 1));
        List<Integer> nums3 = new ArrayList(Arrays.asList(-1, -1, -1));
        List<Integer> nums4 = new ArrayList(Arrays.asList());
        List<Integer> nums5 = new ArrayList(Arrays.asList(-3, -3, 3, 3));
        List<Integer> nums6 = new ArrayList(Arrays.asList(-1, -29, -3, -99, -1));
        List<Integer> nums7 = new ArrayList(Arrays.asList(45, 99, 90, 28, 13, 999, 0));
        NoNeg object = new NoNeg();
        System.out.println(object.noNeg(nums1));
        System.out.println(object.noNeg(nums2));
        System.out.println(object.noNeg(nums3));
        System.out.println(object.noNeg(nums4));
        System.out.println(object.noNeg(nums5));
        System.out.println(object.noNeg(nums6));
        System.out.println(object.noNeg(nums7));
    }
}
