/**
 * Created by patryk on 04.06.2017.
 */
/*Given an array of ints, return the number of times that two 6's are next to each other in the array.
Also count instances where the second "6" is actually a 7.

        array667([6, 6, 2]) → 1
        array667([6, 6, 2, 6]) → 1
        array667([6, 7, 2, 6]) → 1*/

public class Solution11 {

    public int array667(int[] nums) {
        int counter = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == 6) {
                if (nums[i+1] == 7 || nums[i + 1] == 6) {
                    counter++;
                }

            }
        }
        return counter;
    }

    public static void main(String[] args) {
        Solution11 solution3 = new Solution11();
        System.out.println(solution3.array667(new int[]{6, 6, 6,7,4,66,32,6,7}));
    }
}
