import java.util.*;

/**
 * Created by patryk on 04.06.2017.
 */
public class Solution3 {

    public Set<Integer> createIntegersSet(int min, int max, int divisor) {
        try {
            if (divisor == 0 || min > max) {
                throw new IllegalArgumentException("Divisor can't be equals to 0");
            }
            int value = 0;
            Set<Integer> set = new HashSet<>();
            for (int i = min; i < max; i++) {
                value++;
                if (value % divisor == 0) {
                    set.add(value);
                }
            }
        return set;
        } catch (IllegalArgumentException exp) {
            exp.printStackTrace();
        }
        return null;
    }

    public Collection<Integer> convertToDescending(Set<Integer> values) {
        List list = new ArrayList(values);
        Collections.sort(list, Collections.reverseOrder());
        Set resultSet = new LinkedHashSet(list);
        return resultSet;
    }

    public static void main(String[] args) {
        Solution3 solution3 = new Solution3();
        System.out.println(solution3.createIntegersSet(2, 19, 3));
        System.out.println(solution3.convertToDescending(solution3.createIntegersSet(2, 19, 3)));
    }
}
