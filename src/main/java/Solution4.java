/**
 * Created by patryk on 04.06.2017.
 */
/*Given an array of ints, we'll say that a triple is a value appearing 3 times in a row in the array. Return true if the array does not contain any triples.

noTriples([1, 1, 2, 2, 1]) → true
noTriples([1, 1, 2, 2, 2, 1]) → false
noTriples([1, 1, 1, 2, 2, 2, 1]) → false*/
public class Solution4 {
    public boolean noTriples(int[] nums) {
        int counter = 0;
        if(nums.length<3){
            return true;
        }
//        Note that I'm using nums.length - 2, therefore I'm able to consider nums[i+2] elements
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i] == nums[i + 1] && nums[i] == nums[i + 2]) {
                counter++;
            }
        }
        if (counter != 0) {
            return false;
        } else {
            return true;
        }
    }

    public static void main(String[] args) {
        Solution4 solution4 = new Solution4();
        System.out.println(solution4.noTriples(new int[]{1, 1, 2, 2, 2, 1}));
        System.out.println(solution4.noTriples(new int[]{1, 1, 2, 2, 1}));
        System.out.println(solution4.noTriples(new int[]{1, 1}));
        System.out.println(solution4.noTriples(new int[]{1, 1, 1}));
    }
}
