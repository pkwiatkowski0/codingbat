package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of integers, return a list where each integer is multiplied with itself.

square([1, 2, 3]) → [1, 4, 9]
square([6, 8, -6, -8, 1]) → [36, 64, 36, 64, 1]
square([]) → []
*/
public class Square {

    public List<Integer> square(List<Integer> nums) {
        return nums.stream().map(n -> n * n).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList(Arrays.asList(6, -3, 12, 23, 4, 1, 19, 11, 2, 3, 2));
        Square functional2 = new Square();
        System.out.println("Does list actually empty? " + functional2.square(ints).isEmpty());
        System.out.println(functional2.square(ints));
    }
}
