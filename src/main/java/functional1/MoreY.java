package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*Given a list of strings, return a list where each string has "y" added at its start and end.

moreY(["a", "b", "c"]) → ["yay", "yby", "ycy"]
moreY(["hello", "there"]) → ["yhelloy", "ytherey"]
moreY(["yay"]) → ["yyayy"]*/
public class MoreY {
    public List<String> moreY(List<String> strings) {
        return strings.stream().map(n -> "y" + n + "y").collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("a", "bb", "ccc"));
        List<String> strings2 = new ArrayList(Arrays.asList("hello", "there"));
        List<String> strings3 = new ArrayList(Arrays.asList("*"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("kittens", "and", "chocolate", "and"));
        List<String> strings6 = new ArrayList(Arrays.asList("empty", "string", ""));
        MoreY functional5 = new MoreY();
        System.out.println(functional5.moreY(strings1));
        System.out.println(functional5.moreY(strings2));
        System.out.println(functional5.moreY(strings3));
        System.out.println(functional5.moreY(strings4));
        System.out.println(functional5.moreY(strings5));
        System.out.println(functional5.moreY(strings6));
    }
}
