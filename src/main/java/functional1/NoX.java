package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of strings, return a list where each string has all its "x" removed.

noX(["ax", "bb", "cx"]) → ["a", "bb", "c"]
noX(["xxax", "xbxbx", "xxcx"]) → ["a", "bb", "c"]
noX(["x"]) → [""]
*/
public class NoX {
    public List<String> noX(List<String> strings) {
        return strings.stream().map(n -> n.replace("x", "")).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("Ax", "BxxBx", "CxCCx"));
        List<String> strings2 = new ArrayList(Arrays.asList("xxxHELLOxxxx", "THxxExxxRxE"));
        List<String> strings3 = new ArrayList(Arrays.asList("KxitxteN", "ChocolxaTE"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("aaX", "bYbx", "Ycc", "ZZZ"));
        List<String> strings6 = new ArrayList(Arrays.asList("EMxPTY", "stRxxxxIng", ""));
        NoX functional8 = new NoX();
        System.out.println(functional8.noX(strings1));
        System.out.println(functional8.noX(strings2));
        System.out.println(functional8.noX(strings3));
        System.out.println(functional8.noX(strings4));
        System.out.println(functional8.noX(strings5));
        System.out.println(functional8.noX(strings6));
    }
}
