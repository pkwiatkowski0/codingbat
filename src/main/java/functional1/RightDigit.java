package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*Given a list of non-negative integers, return an integer list of the rightmost digits. (Note: use %)

rightDigit([1, 22, 93]) → [1, 2, 3]
rightDigit([16, 8, 886, 8, 1]) → [6, 8, 6, 8, 1]
rightDigit([10, 0]) → [0, 0]
*/
public class RightDigit {
    public List<Integer> rightDigit(List<Integer> nums) {
//do poprawy
        return nums.stream().map(n -> n.lowestOneBit(n.intValue())).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> nums1 = new ArrayList(Arrays.asList(1, 22, 93));
        List<Integer> nums2 = new ArrayList(Arrays.asList(16, 8, 886, 8, 1));
        List<Integer> nums3 = new ArrayList(Arrays.asList(10, 0));
        List<Integer> nums4 = new ArrayList(Arrays.asList());
        List<Integer> nums5 = new ArrayList(Arrays.asList(5, 10));
        List<Integer> nums6 = new ArrayList(Arrays.asList(-1, -2, -3, -2, -1));
        List<Integer> nums7 = new ArrayList(Arrays.asList(6, -3, 125, 23, 4, 1, 19, 117, 22, 3, 22));
        RightDigit functional9 = new RightDigit();
        System.out.println(functional9.rightDigit(nums1));
        System.out.println(functional9.rightDigit(nums2));
        System.out.println(functional9.rightDigit(nums3));
        System.out.println(functional9.rightDigit(nums4));
        System.out.println(functional9.rightDigit(nums5));
        System.out.println(functional9.rightDigit(nums6));
        System.out.println(functional9.rightDigit(nums7));
    }
}
