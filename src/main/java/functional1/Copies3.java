package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
public class Copies3 {
    public List<String> copies3(List<String> strings) {
        return strings.stream().map(n -> n + n + n).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("a", "bb", "ccc"));
        List<String> strings2 = new ArrayList(Arrays.asList("hello", "there"));
        List<String> strings3 = new ArrayList(Arrays.asList("*"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("kittens", "and", "chocolate", "and"));
        List<String> strings6 = new ArrayList(Arrays.asList("empty", "string", ""));
        Copies3 functional4 = new Copies3();
        System.out.println(functional4.copies3(strings1));
        System.out.println(functional4.copies3(strings2));
        System.out.println(functional4.copies3(strings3));
        System.out.println(functional4.copies3(strings4));
        System.out.println(functional4.copies3(strings5));
        System.out.println(functional4.copies3(strings6));
    }
}
