package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*Given a list of strings, return a list where each string has "*" added at its end.

addStar(["a", "bb", "ccc"]) → ["a*", "bb*", "ccc*"]
addStar(["hello", "there"]) → ["hello*", "there*"]
addStar(["*"]) → ["**"]
*/
public class AddStar {
    List<String> addStar(List<String> strings) {
        return strings.stream().map(n -> n + "*").collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("a", "bb", "ccc"));
        List<String> strings2 = new ArrayList(Arrays.asList("hello", "there"));
        List<String> strings3 = new ArrayList(Arrays.asList("*"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("kittens", "and", "chocolate", "and"));
        List<String> strings6 = new ArrayList(Arrays.asList("empty", "string", ""));
        AddStar functional3 = new AddStar();
        System.out.println(functional3.addStar(strings1));
        System.out.println(functional3.addStar(strings2));
        System.out.println(functional3.addStar(strings3));
        System.out.println(functional3.addStar(strings4));
        System.out.println(functional3.addStar(strings5));
        System.out.println(functional3.addStar(strings6));
    }

}
