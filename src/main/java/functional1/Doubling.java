package functional1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/* Given a list of integers, return a list where each integer is multiplied by 2.

        doubling([1, 2, 3]) → [2, 4, 6]
        doubling([6, 8, 6, 8, -1]) → [12, 16, 12, 16, -2]
        doubling([]) → [] */
public class Doubling {
    public List<Integer> doubling(List<Integer> nums) {
        return nums.stream().map(n -> n * 2).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        Doubling functional1 = new Doubling();
        System.out.println("Does testEmptyList actually empty? " + functional1.doubling(ints).isEmpty());
        int myInt1 = 6;
        int myInt2 = 8;
        int myInt3 = 6;
        int myInt4 = 8;
        int myInt5 = -1;
        ints.add(myInt1);
        ints.add(myInt2);
        ints.add(myInt3);
        ints.add(myInt4);
        ints.add(myInt5);
        System.out.println(functional1.doubling(ints));

    }
}
