package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of integers, return a list where each integer is added to 1 and the result is multiplied by 10.

math1([1, 2, 3]) → [20, 30, 40]
math1([6, 8, 6, 8, 1]) → [70, 90, 70, 90, 20]
math1([10]) → [110]*/
public class Math1 {
    public List<Integer> math1(List<Integer> nums) {
        return nums.stream().map(n -> (n + 1) * 10).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> nums1 = new ArrayList(Arrays.asList(1, 2, 3));
        List<Integer> nums2 = new ArrayList(Arrays.asList(6, 8, 6, 8, 1));
        List<Integer> nums3 = new ArrayList(Arrays.asList(10));
        List<Integer> nums4 = new ArrayList(Arrays.asList());
        List<Integer> nums5 = new ArrayList(Arrays.asList(5, 10));
        List<Integer> nums6 = new ArrayList(Arrays.asList(-1, -2, -3, -2, -1));
        List<Integer> nums7 = new ArrayList(Arrays.asList(6, -3, 12, 23, 4, 1, 19, 11, 2, 3, 2));
        Math1 functional6 = new Math1();
        System.out.println(functional6.math1(nums1));
        System.out.println(functional6.math1(nums2));
        System.out.println(functional6.math1(nums3));
        System.out.println(functional6.math1(nums4));
        System.out.println(functional6.math1(nums5));
        System.out.println(functional6.math1(nums6));
        System.out.println(functional6.math1(nums7));
    }
}
