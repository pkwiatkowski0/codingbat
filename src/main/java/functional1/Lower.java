package functional1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by patryk on 17.06.2017.
 */
/*
Given a list of strings, return a list where each string is converted to lower case (Note: String toLowerCase() method).

lower(["Hello", "Hi"]) → ["hello", "hi"]
lower(["AAA", "BBB", "ccc"]) → ["aaa", "bbb", "ccc"]
lower(["KitteN", "ChocolaTE"]) → ["kitten", "chocolate"]
*/
public class Lower {
    public List<String> lower(List<String> strings) {
        return strings.stream().map(n -> n.toLowerCase()).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList(Arrays.asList("A", "BB", "CCC"));
        List<String> strings2 = new ArrayList(Arrays.asList("HELLO", "THERE"));
        List<String> strings3 = new ArrayList(Arrays.asList("KitteN", "ChocolaTE"));
        List<String> strings4 = new ArrayList(Arrays.asList());
        List<String> strings5 = new ArrayList(Arrays.asList("aaX", "bYb", "Ycc", "ZZZ"));
        List<String> strings6 = new ArrayList(Arrays.asList("EMPTY", "stRIng", ""));
        Lower functional7 = new Lower();
        System.out.println(functional7.lower(strings1));
        System.out.println(functional7.lower(strings2));
        System.out.println(functional7.lower(strings3));
        System.out.println(functional7.lower(strings4));
        System.out.println(functional7.lower(strings5));
        System.out.println(functional7.lower(strings6));
    }
}
