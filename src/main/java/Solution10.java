/**
 * Created by patryk on 04.06.2017.
 */
/*
Given a string and a non-negative int n, return a larger string that is n copies of the original string.

        stringTimes("Hi", 2) → "HiHi"
        stringTimes("Hi", 3) → "HiHiHi"
        stringTimes("Hi", 1) → "Hi"
        */
public class Solution10 {

    public String stringTimes(String str, int n) {
        if(n == 0) {
            return "";
        }
        String result = str;
        for (int i = 1; i < n; i++) {
            result += str;
        }
        return result;
    }

    public static void main(String[] args) {
        Solution10 solution2 = new Solution10();
        System.out.println(solution2.stringTimes("Hi", 6));
        System.out.println(solution2.stringTimes("Hi", 0));
        System.out.println(solution2.stringTimes("Hi", 1));
        System.out.println(solution2.stringTimes("Hi", 10));
        System.out.println(solution2.stringTimes("Hi", 30));
    }
}
