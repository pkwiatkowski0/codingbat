package map1;

import java.util.HashMap;

/**
 * Created by patryk on 19.06.2017.
 */
/*Modify and return the given map as follows: if the key "a" has a value, set the key "b" to have that same value. In all cases remove the key "c", leaving the rest of the map unchanged.

mapShare({"a": "aaa", "b": "bbb", "c": "ccc"}) → {"a": "aaa", "b": "aaa"}
mapShare({"b": "xyz", "c": "ccc"}) → {"b": "xyz"}
mapShare({"a": "aaa", "c": "meh", "d": "hi"}) → {"a": "aaa", "b": "aaa", "d": "hi"}*/
public class MapShare {

    public HashMap<String, String> mapShare(HashMap<String, String> map) {
        if (map.containsKey("a")) {
            map.put("b", map.get("a"));
        }
        if (map.containsKey("c")) {
            map.remove("c");
        }
        return map;
    }

    public static void main(String[] args) {
        HashMap<String, String> map1 = new HashMap<>();
        HashMap<String, String> map2 = new HashMap<>();
        HashMap<String, String> map3 = new HashMap<>();
        MapShare object = new MapShare();
        map1.put("a", "aaa");
        map1.put("b","bbb");
        map1.put("c","ccc");
        map2.put("b", "xyz");
        map2.put("c", "ccc");
        map3.put("a", "aaa");
        map3.put("c", "meh");
        map3.put("d", "hi");
        System.out.println(object.mapShare(map1));
        System.out.println(object.mapShare(map2));
        System.out.println(object.mapShare(map3));
    }
}
