package map1;

import java.util.HashMap;

/**
 * Created by patryk on 19.06.2017.
 */
/*Modify and return the given map as follows: if the key "a" has a value, set the key "b" to have that value, and set the key "a" to have the value "". Basically "b" is a bully, taking the value and replacing it with the empty string.

mapShare({"a": "candy", "b": "dirt"}) → {"a": "", "b": "candy"}
mapShare({"a": "candy"}) → {"a": "", "b": "candy"}
mapShare({"a": "candy", "b": "carrot", "c": "meh"}) → {"a": "", "b": "candy", "c": "meh"}*/
public class MapBully {
    public HashMap<String, String> mapBully(HashMap<String, String> map) {
        if (map.containsKey("a")) {
            map.put("b", map.get("a"));
            map.put("a", "");
        }
        return map;
    }


    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        MapBully object = new MapBully();

        map.put("a", "candy");
//        System.out.println(object.mapShare(map));

        map.put("b", "dirt");
        System.out.println(object.mapBully(map));

        map.put("c", "meh");
//        System.out.println(object.mapShare(map));
    }

}
