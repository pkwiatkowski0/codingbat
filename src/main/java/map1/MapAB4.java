package map1;

import java.util.HashMap;

/**
 * Created by patryk on 20.06.2017.
 */
/*
Modify and return the given map as follows: if the keys "a" and "b" have values that have different lengths, then set "c" to have the longer value. If the values exist and have the same length, change them both to the empty string in the map.

mapAB4({"a": "aaa", "b": "bb", "c": "cake"}) → {"a": "aaa", "b": "bb", "c": "aaa"}
mapAB4({"a": "aa", "b": "bbb", "c": "cake"}) → {"a": "aa", "b": "bbb", "c": "bbb"}
mapAB4({"a": "aa", "b": "bbb"}) → {"a": "aa", "b": "bbb", "c": "bbb"}*/
public class MapAB4 {
    public HashMap<String, String> mapAB4(HashMap<String, String> map) {
        if (map.containsKey("a") && map.containsKey("b")) {
            if (map.get("a").length() == map.get("b").length()) {
                map.put("a", "");
                map.put("b", "");
            }
            if (map.get("a").length() > map.get("b").length()) {
                map.put("c", map.get("a"));
            } else if (map.get("a").length() < map.get("b").length()) {
                map.put("c", map.get("b"));
            }
        } else {
            return map;
        }

        return map;
    }

    public static void main(String[] args) {
        MapAB4 object = new MapAB4();
        HashMap<String, String> map1 = new HashMap<>();
        HashMap<String, String> map2 = new HashMap<>();
        HashMap<String, String> map3 = new HashMap<>();
        HashMap<String, String> map4 = new HashMap<>();
        HashMap<String, String> map5 = new HashMap<>();
        HashMap<String, String> map6 = new HashMap<>();
        HashMap<String, String> map7 = new HashMap<>();
        HashMap<String, String> map8 = new HashMap<>();
        HashMap<String, String> map9 = new HashMap<>();
        HashMap<String, String> map10 = new HashMap<>();
        HashMap<String, String> map11 = new HashMap<>();
        HashMap<String, String> map12 = new HashMap<>();
        HashMap<String, String> map13 = new HashMap<>();
        HashMap<String, String> map14 = new HashMap<>();
        HashMap<String, String> map15 = new HashMap<>();

        map1.put("a", "aaa");
        map1.put("b", "bb");
        map1.put("c", "cake");

        map2.put("a", "aa");
        map2.put("b", "bbb");
        map2.put("c", "cake");

        map3.put("a", "aa");
        map3.put("b", "bbb");

        map4.put("a", "aaa");

        map5.put("a", "bbb");

        map6.put("a", "aaa");
        map6.put("b", "bbb");
        map6.put("c", "cake");

        map7.put("a", "a");
        map7.put("b", "b");
        map7.put("c", "cake");

        map8.put("a", "");
        map8.put("b", "b");
        map8.put("c", "cake");

        map9.put("a", "a");
        map9.put("b", "");
        map9.put("c", "cake");

        map10.put("c", "cat");
        map10.put("d", "dog");

        map11.put("ccc", "ccc");

        map12.put("c", "a");
        map12.put("d", "b");

        map13.put("", "");

        map14.put("a", "");
        map14.put("z", "z");

        map15.put("b", "");
        map15.put("z", "z");

        object.showMap(map1);
        object.showMap(map2);
        object.showMap(map3);
        object.showMap(map4);
        object.showMap(map5);
        object.showMap(map6);
        object.showMap(map7);
        object.showMap(map8);
        object.showMap(map9);
        object.showMap(map10);
        object.showMap(map11);
        object.showMap(map12);
        object.showMap(map13);
        object.showMap(map14);
        object.showMap(map15);
    }
    public void showMap(HashMap<String, String> map){
        MapAB4 presenter = new MapAB4();
        System.out.println(presenter.mapAB4(map));
    }
}
