package map1;

import java.util.HashMap;

/**
 * Created by patryk on 20.06.2017.
 */
/*
Modify and return the given map as follows: if the keys "a" and "b" are both in the map and have equal values, remove them both.

mapAB4({"a": "aaa", "b": "aaa", "c": "cake"}) → {"c": "cake"}
mapAB4({"a": "aaa", "b": "bbb"}) → {"a": "aaa", "b": "bbb"}
mapAB4({"a": "aaa", "b": "bbb", "c": "aaa"}) → {"a": "aaa", "b": "bbb", "c": "aaa"}*/
public class MapAB2 {
    public HashMap<String, String> mapAB2(HashMap<String, String> map){
        if(map.containsKey("a") && map.containsKey("b") && map.get("a").equals(map.get("b"))) {
            map.remove("a");
            map.remove("b");
        }
        return map;
    }

    public static void main(String[] args) {
        MapAB2 object = new MapAB2();
        HashMap<String, String> map1 = new HashMap<>();
        HashMap<String, String> map2 = new HashMap<>();
        HashMap<String, String> map3 = new HashMap<>();

        map1.put("a", "aaa");
        map1.put("b", "aaa");
        map1.put("c", "cake");

        map2.put("b", "bbb");
        map2.put("a", "bbb");

        map3.put("a", "aaa");
        map3.put("b", "bbb");
        map3.put("c", "aaa");

        System.out.println(object.mapAB2(map1));
        System.out.println(object.mapAB2(map2));
        System.out.println(object.mapAB2(map3));
    }
}
