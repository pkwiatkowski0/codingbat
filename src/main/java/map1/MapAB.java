package map1;

import java.util.HashMap;

/**
 * Created by patryk on 20.06.2017.
 */
/*Modify and return the given map as follows: for this problem the map may or may not contain the "a" and "b" keys. If both keys are present, append their 2 string values together and store the result under the key "ab".

mapAB4({"a": "Hi", "b": "There"}) → {"a": "Hi", "ab": "HiThere", "b": "There"}
mapAB4({"a": "Hi"}) → {"a": "Hi"}
mapAB4({"b": "There"}) → {"b": "There"}*/
public class MapAB {
    public HashMap<String, String> mapAB(HashMap<String, String> map){
        if(map.containsKey("a") && map.containsKey("b")) {
            map.put("ab", map.get("a") + map.get("b"));
        }

        return map;
    }

    public static void main(String[] args) {
        MapAB object = new MapAB();
        HashMap<String, String> map1 = new HashMap<>();
        HashMap<String, String> map2 = new HashMap<>();
        HashMap<String, String> map3 = new HashMap<>();

        map1.put("a", "Hi");
        map1.put("b", "There");
        map2.put("a", "Hi");
        map3.put("b", "There");

        System.out.println(object.mapAB(map1));
        System.out.println(object.mapAB(map2));
        System.out.println(object.mapAB(map3));
    }
}
