import java.util.HashMap;
import java.util.Map;

/**
 * Created by patryk on 04.06.2017.
 */
/*Given an array of strings, return a Map<String, Integer>
 containing a key for every different string in the array, and the value is that string's length.

wordLen(["a", "bb", "a", "bb"]) → {"bb": 2, "a": 1}
wordLen(["this", "and", "that", "and"]) → {"that": 4, "and": 3, "this": 4}
wordLen(["code", "code", "code", "bug"]) → {"code": 4, "bug": 3}*/
public class Solution7 {
    public Map<String, Integer> wordLen(String[] strings) {

        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < strings.length; i++) {
            map.put(strings[i], strings[i].length());
        }
        return map;
    }

    public static void main(String[] args) {
        Solution6 solution6 = new Solution6();
        System.out.println(solution6.word0(new String[]{"a", "b", "a", "b"}));
        System.out.println(solution6.word0(new String[]{"a", "bb", "a", "bb"}));
        System.out.println(solution6.word0(new String[]{"this", "and", "that", "and"}));
        System.out.println(solution6.word0(new String[]{"code", "code", "code", "bug"}));
        System.out.println(solution6.word0(new String[]{"code", "code", "code", "bug", "bug"}));
    }
}
